<?php
require_once('functions.php');
session_start();

if (isset($_POST['register'])) {
    register();
} else if (isset($_POST['user'])) {
    login();
} else if (isset($_POST['logout'])) {
    unset($_SESSION['username']);
}

$currentUser = getCurrentuser();

if (!$currentUser) { ?>
    <div>
        Register:
        <form method="post">
            <input type="hidden" name="register" value="1"/>
            Username: <input name="user" placeholder="Username"><br/>
            Password: <input name="pass" placeholder="Password"><br/>
            <input type="submit" value="Register">
        </form>
    </div>
    <div>
        Login:
        <form method="post">
            Username: <input name="user" placeholder="Username"><br/>
            Password: <input name="pass" placeholder="Password"><br/>
            <input type="submit" value="Login">
        </form>
    </div><?php
} else { ?>
    <div>
        Welcome back, <?= htmlspecialchars($currentUser['username']) ?>!<br/>
        <?= $currentUser['description'] ?>
    </div>
    <form method="post">
        <input type="submit" name="logout" value="Logout">
    </form>
    <?php
}
