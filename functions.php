<?php
require_once('db.php');

function getCurrentUser()
{
    if (!isset($_SESSION['username'])) {
        return null;
    }
    return selectUser($_SESSION['username']);
}

function selectUser($username)
{
    global $db;

    $query = $db->prepare('SELECT * FROM users WHERE username = :username');
    $query->bindParam(':username', $username);

    $query->execute();
    $result = $query->fetch();

    return $result;
}

function login()
{
    global $db;
    if (!isset($_POST['user']) ||
        !isset($_POST['pass']) ||
        !is_string($_POST['user']) ||
        !is_string($_POST['pass'])) {
        return false;
    }

    $query = $db->prepare('SELECT * FROM users WHERE username = :username and password = :password');
    $query->bindParam(':username', $_POST['user']);
    $query->bindParam(':password', $_POST['pass']);

    $query->execute();
    $result = $query->fetch();
    if ($result) {
        $_SESSION['username'] = $result['username'];
        echo 'Logged in succesfully!';
    } else {
        echo 'Invalid credentials!';
    }
}

function register()
{
    global $db;
    if (!isset($_POST['user']) ||
        !isset($_POST['pass']) ||
        !is_string($_POST['user']) ||
        !is_string($_POST['pass'])) {
        return false;
    }
    if (selectUser($_POST['user'])) {
        echo 'Username already taken!';
        return false;
    }

    $query = $db->prepare('INSERT INTO users (username, password, description) values (:username, :password, "Regular user")');
    $query->bindParam(':username', $_POST['user']);
    $query->bindParam(':password', $_POST['pass']);
    $query->execute();
    echo 'Registered succesfully! Please login';
}
